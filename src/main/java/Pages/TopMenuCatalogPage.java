package Pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class TopMenuCatalogPage {

    public static SelenideElement dachaSadOgorodMainCategory = $x("//a[contains(@href,'dacha') and contains(@class,'js-menu-categories__link')]");
    public static SelenideElement hosesSubCategory = $x("//a[@class='menu__link' and contains(@href,'hoses')]");
    public static SelenideElement kidsMainCategory = $x("//a[contains(@href,'kids') and contains(@class,'js-menu-categories__link')]");
    public static SelenideElement kidsFoodSubCategory = $x("//a[@class='menu__link' and contains(@href,'detskie-smesi')]");
}
