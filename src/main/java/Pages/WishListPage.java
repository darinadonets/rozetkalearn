package Pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class WishListPage {

    public static SelenideElement wishButton = $x("//a[contains(@class,'button_type_wish')]");
    public static SelenideElement wishButtonCounter = $("span.header-actions__button-counter");
    public static SelenideElement deleteWishListButton = $x("//a[@name='wishlist-delete']");
    public static ElementsCollection deleteWishListButtons = $$("a[class$='remove']");
    public static SelenideElement deleteWishListSubmitButton = $x("//a[@name='wishlist-delete-submit']");
    public static SelenideElement wishListTitle = $(".wishlist-firstlist-title");
}
