package Pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.appears;
import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selenide.$;

public class LogInPage {

    public static SelenideElement emailField = $("#auth_email");
    public static SelenideElement passField = $("#auth_pass");
    public static SelenideElement submitButton = $("button.auth-modal__submit");
    public static SelenideElement errorMessageField = $(".error-message");

    public static void logIn (String email, String password) {
        emailField.setValue(email);
        passField.setValue(password);
        submitButton.should(appears, enabled).click();
    }

    public static void logIn() {
        logIn("darinka95@ukr.net", "Lollipop666");
    }

}
