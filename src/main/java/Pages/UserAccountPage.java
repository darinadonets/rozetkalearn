package Pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class UserAccountPage {

    public static SelenideElement signOutButton = $(".profile-m-edit-signout");
    public static SelenideElement afterLoginAccountLinks = $(".main-auth__links");
}
