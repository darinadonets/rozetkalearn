package Pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class TopMenuPage {


    public static SelenideElement userLink = $("a.header-topline__user-link");
    public static SelenideElement searchInput = $("input.search-form__input");
    public static SelenideElement searchButton = $("button.search-form__submit");
    public static SelenideElement cartIcon = $(".header-actions__button_type_basket");
    public static SelenideElement covidPromoLink = $x("//a[contains(@href,'covid')]");
    public static ElementsCollection wishCounter = $$("app-goods-wishlist .wish-button");
    public static SelenideElement catalogButton = $x("//span[@class='menu-toggler__text']");
}
