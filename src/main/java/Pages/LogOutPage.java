package Pages;

import static Pages.LogInPage.emailField;
import static Pages.TopMenuPage.userLink;
import static Pages.UserAccountPage.signOutButton;
import static com.codeborne.selenide.Condition.appear;

public class LogOutPage {

    public static void logOut() {
        signOutButton.should(appear).click();
        userLink.click();
        emailField.should(appear);
    }
}
