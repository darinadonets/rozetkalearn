package Pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ProductsListerPage {

    public static SelenideElement productSearchPlaceHolder = $("div.goods-tile__inner");
    //    public static ElementsCollection productSearchPlaceHolders = $$("div.goods-tile__inner");
    public static List<SelenideElement> productSearchPlaceHolders = $$("div.goods-tile__inner");
    public static SelenideElement buyProductButton = $("button.buy-button");

    public static List<String> getTitlesProductsListFromPlaceHolders() {
//         List<String> list = productSearchPlaceHolders.texts();
        List<String> list = productSearchPlaceHolders.stream()
                .map(el -> el.$("a span.goods-tile__title")
                        .should(Condition.appear)
                        .getText())
                .collect(Collectors.toList());
        return list;
    }

}
