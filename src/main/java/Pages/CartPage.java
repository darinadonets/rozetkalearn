package Pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {


    public static SelenideElement cartPopUp = $(".cart-modal__list");
    public static SelenideElement cartPopUpCloseButton = $("button.modal__close");
    public static SelenideElement cartList = $(".cart-modal__list");
}
