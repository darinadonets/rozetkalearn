package utils.ui;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import static com.codeborne.selenide.Configuration.browserCapabilities;
import static com.codeborne.selenide.Configuration.remote;

public class WebdriverSetting {

    public static void setUp() {
        Configuration.baseUrl = "https://rozetka.com.ua/";
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        Configuration.screenshots = false;
    }
}
