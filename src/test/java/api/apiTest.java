package api;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class apiTest{

    @Test
    public void newTest() {
//        given().baseUri("https://jsonplaceholder.typicode.com")
//                .when().get("/users")
//                .then().body("id[0]",equalTo(1));
//
//        Response first = given().baseUri("https://jsonplaceholder.typicode.com")
//                .when().get("/users");

        Response secondUser = given().baseUri("https://jsonplaceholder.typicode.com")
                .when().get("/users/2");

//        Deserialization / Serialization  with help of AS method
        UserDto mySecondUser = secondUser.as( UserDto.class); //new UserDto( id, name, email ) from response json

        secondUser.then()
                .statusCode(200)
//                .body("id[0]",equalTo(2))
//                .body("id[0]",equalTo(2))
                .body("id",equalTo(2));


        Assert.assertEquals(mySecondUser.getId(), 2);
//        List<UserDto> myDto = Arrays.asList(first.as(UserDto[].class));

        System.out.println();
    }

    @Test
    public void newDto() {
        Response users = given().baseUri("https://jsonplaceholder.typicode.com")
                .when().get("/posts");
        List<UserDto> myUsers = Arrays.asList(users.as(UserDto[].class));

//        firstUser.then()
//                .statusCode(200)
//                .body("userId",contains(1));
        Assert.assertEquals(myUsers.get(1).getId(), 2);

    }


}
