package ui.before;

import org.testng.annotations.BeforeMethod;

import static Pages.LogInPage.logIn;
import static Pages.TopMenuPage.userLink;

public class Before extends BeforeWithoutLogin{

    @BeforeMethod
    public void beforeMethod() {
        super.beforeMethod();
        userLink.click();
        logIn();
    }
}
