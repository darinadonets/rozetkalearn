package ui.before;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import utils.ui.WebdriverSetting;

import static com.codeborne.selenide.Selenide.open;

public class BeforeWithoutLogin {

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("Start without login");
        WebdriverSetting.setUp();
        open("");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("Test end");
        Selenide.closeWebDriver();
    }
}
