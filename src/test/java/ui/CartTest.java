package ui;

import ui.before.BeforeWithoutLogin;
import org.testng.annotations.Test;

import static Pages.CartPage.*;
import static Pages.ProductsListerPage.buyProductButton;
import static Pages.ProductsListerPage.productSearchPlaceHolder;
import static Pages.TopMenuPage.*;
import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.enabled;

public class CartTest extends BeforeWithoutLogin {

    @Test(groups = { "functest"})
    public void verifyProductInBasket() {
        searchInput.setValue("Doll");
        searchButton.click();
        productSearchPlaceHolder.should(appear);
        buyProductButton.click();
        cartPopUp.should(appear);
        cartPopUpCloseButton.should(enabled).hover().click();
        cartIcon.click();
        cartList.should(appear);
    }
}
