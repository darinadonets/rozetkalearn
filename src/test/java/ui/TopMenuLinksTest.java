package ui;

import com.codeborne.selenide.SelenideElement;
import ui.before.BeforeWithoutLogin;
import com.codeborne.selenide.Condition;
import org.testng.annotations.Test;

import java.util.List;
import java.util.stream.Collectors;

import static Pages.ArticklePage.covidArtickleTitle;
import static Pages.TopMenuPage.covidPromoLink;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.element;

public class TopMenuLinksTest extends BeforeWithoutLogin {

    @Test
    public void openPromoCovid19() {
        covidPromoLink.click();
        covidArtickleTitle.should(Condition.appear);
    }

    @Test
    public void getAllCategoriesList() {
        covidPromoLink.click();
        List<SelenideElement> headerRowTop = $$(".body-header-row-top ul li");
//        strim of all catagories
        List<String> listOfHeaders = headerRowTop.stream().map(el -> el.getText()).collect(Collectors.toList());
        System.out.println();

    }
}
