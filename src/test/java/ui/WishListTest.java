package ui;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ui.before.Before;

import static Pages.ProductsListerPage.productSearchPlaceHolder;
import static Pages.TopMenuPage.searchInput;
import static Pages.TopMenuPage.wishCounter;
import static Pages.WishListPage.*;
import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.enabled;

public class WishListTest extends Before {

    @BeforeMethod
    public void beforeMethod() {
        super.beforeMethod();
        if (!wishButtonCounter.isDisplayed()) {
        } else {
            wishButton.should(appear).click();
            deleteWishListButton.click();
            deleteWishListSubmitButton.should(enabled).click();
            wishListTitle.should(appear);
        }
    }

    @Test
    public void addProductToWishList() {
        searchProduct("toy");
        wishCounter.get(3).click();
        Selenide.refresh();
        wishCounter.get(5).click();
        Assert.assertEquals("2", wishButton.$("span").getText());
        wishButton.click();
        Selenide.sleep(300);
        Assert.assertEquals(2, deleteWishListButtons.size());
    }

    public void searchProduct(String productForSearch) {
        Selenide.sleep(2000);
        searchInput.should(appear).setValue(productForSearch).pressEnter();
        productSearchPlaceHolder.should(Condition.appear);
    }
}

