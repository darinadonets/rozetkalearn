package ui.logIn;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ui.before.BeforeWithoutLogin;

import static Pages.LogInPage.errorMessageField;
import static Pages.LogInPage.logIn;
import static Pages.LogOutPage.logOut;
import static Pages.TopMenuPage.userLink;
import static Pages.UserAccountPage.afterLoginAccountLinks;
import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.appears;

public class LogInAndLogOutTest extends BeforeWithoutLogin {

    @BeforeMethod
    public void beforeMethod() {
        super.beforeMethod();
        System.out.println("Start check");
    }

    @Test
    public void logInWithWrongPassword() {
        userLink.click();
        logIn("darinka95@ukr.net", "lollipop");
        errorMessageField.should(appear);
    }

    @Test()
    public void logInWithDefaultUser() {
        userLink.click();
        logIn();
        afterLoginAccountLinks.should(appears);
        userLink.hover().click();
        logOut();
    }
}
