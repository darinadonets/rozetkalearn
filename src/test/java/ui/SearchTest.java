package ui;

import com.codeborne.selenide.Condition;
import org.testng.annotations.Test;
import ui.before.BeforeWithoutLogin;

import java.util.List;

import static Pages.ProductsListerPage.getTitlesProductsListFromPlaceHolders;
import static Pages.ProductsListerPage.productSearchPlaceHolder;
import static Pages.TopMenuPage.searchButton;
import static Pages.TopMenuPage.searchInput;
import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.enabled;
import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.assertEquals;

public class SearchTest extends BeforeWithoutLogin {

    @Test
    public void searchProduct() {
        searchInput.setValue("Doll");
        searchButton.click();
        productSearchPlaceHolder.should(Condition.appear);
    }

    @Test
    public void searchProducts() {
        searchInput.setValue("Doll");
        searchButton.click();
        productSearchPlaceHolder.should(appear, enabled);
        assertEquals(getTitlesProductsListFromPlaceHolders().size(), 36);
        assertThat(getTitlesProductsListFromPlaceHolders()).hasSize(36); //AssertJ

        List<String> listOfTitles = getTitlesProductsListFromPlaceHolders();
        assertThat(listOfTitles).matches(e -> !e.contains("hrsduy"));
        assertThat(getTitlesProductsListFromPlaceHolders())
                .as("NOT found PROPER TITLE")
                .contains("Барьерка для лестниц и дверей Dolle Svea");


    }


}
