package ui;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;
import ui.before.BeforeWithoutLogin;

import static Pages.TopMenuCatalogPage.*;
import static Pages.TopMenuPage.catalogButton;
import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Selenide.$x;

public class CatalogTest extends BeforeWithoutLogin {

    private SelenideElement hosesTitle = $x("//h1[@class='catalog-heading' and contains(text(),'Шланги')]");
    private SelenideElement kidsFoodTitle = $x("//h1[@class='catalog-heading' and contains(text(),'Детские смеси')]");

    @Test
    public void searchHosesInSabCategoryInCatalog() {
        catalogButton.click();
        dachaSadOgorodMainCategory.hover();
        hosesSubCategory.click();
        hosesTitle.should(appear);
    }

    @Test
    public void searchDetskieSmesiInSabCategoryInCatalog() {
        catalogButton.click();
        kidsMainCategory.hover();
        kidsFoodSubCategory.click();
        kidsFoodTitle.should(appear);
    }

}
