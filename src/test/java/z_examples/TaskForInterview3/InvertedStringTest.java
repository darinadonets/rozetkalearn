package z_examples.TaskForInterview3;

import org.testng.annotations.Test;

import static z_examples.TaskForInterview3.InvertedString.reverseString;

public class InvertedStringTest {

    @Test
    public void invertString() {

        String stringForReverse = "Love you babe";
        StringBuilder sb = new StringBuilder(stringForReverse);
        System.out.println(sb.reverse());
    }


    @Test
    public void reverseStringTest() {
//        String my = InvertedString.reverseString("My lovely day");
        String my = reverseString("My lovely day");
        System.out.println(my);

        String cap = reverseString("ChanGE_CaSe", true);
        String low = reverseString("ChanGE_CaSe", false);
        System.out.println();
    }


}