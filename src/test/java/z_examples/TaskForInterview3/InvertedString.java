package z_examples.TaskForInterview3;

public class InvertedString {

    public static String reverseString(String str) {
        String res = "";

        for (int i = str.length() - 1; i >= 0; i--) {
            res = res + str.charAt(i);
        }
        return res;
    }

    public static String reverseString(String str, boolean capitalize) {
        String revStr = reverseString(str);
        return capitalize ? revStr.toUpperCase() : revStr.toLowerCase();
    }
}
