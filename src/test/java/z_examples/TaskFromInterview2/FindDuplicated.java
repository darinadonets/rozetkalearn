package z_examples.TaskFromInterview2;

import org.testng.annotations.Test;

import java.sql.SQLOutput;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FindDuplicated {

    @Test
    public void findDuplicatedElementsInList() {
        List<Integer> linkedList = new LinkedList<>();
        linkedList.add(5);
        linkedList.add(6);
        linkedList.add(6);
        linkedList.add(44);

        for (int i = 0; i < linkedList.size(); i++) {
            for (int j = i + 1; j < linkedList.size(); j++) {
                if (linkedList.get(i).equals(linkedList.get(j))) {
                    System.out.println(linkedList.get(i));
                }
            }
        }

        System.out.println("\nExample 2 - Count all with frequency");
        Set<Integer> uniqueSet = new HashSet<>(linkedList);
        for (Integer temp : uniqueSet) {
            System.out.println(temp + ": " + Collections.frequency(linkedList, temp));
        }
    }
}

