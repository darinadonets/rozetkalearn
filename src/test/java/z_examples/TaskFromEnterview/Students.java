package z_examples.TaskFromEnterview;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Students {

    List<Student> faculty;
    public Integer bestStudentsCount = 5;

    public Students() {
        faculty = new LinkedList<>();
        faculty.add(new Student("Maria", 99));
        faculty.add(new Student("Viktoria", 83));
        faculty.add(new Student("Mary", 44));
        faculty.add(new Student("Dana", 55));
        faculty.add(new Student("Boris", 87));
        faculty.add(new Student("Ivan", 81));
        faculty.add(new Student("Ivan", 85));
        faculty.add(new Student("Klo", 89));
    }

    public List<Student> getSortedFacultyList() {
        Comparator<Student> compareByMark = (Student o1, Student o2) ->
                o1.getScore().compareTo(o2.getScore());
        Collections.sort(faculty, compareByMark.reversed());
        return faculty;
    }
}