package z_examples.types;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TVtest {

    @Test
    public void firstTV() {
        TV samsung = new TV("samsung", 15);
        TV lg = new TV("lg", 25);

//        samsung.turn(OFF);
//
//        lg.turn(ON)
//                .turn(ON)
//                .turn(OFF)
//                .setChannel(23)
//                .turn(OFF);
//
//        samsung.setChannel(15)
//                .getChannel();

        //TODO names of TV brand to array. Stream in it
        List<TV> listOfTv = new ArrayList();
        listOfTv.add(samsung);
        listOfTv.add(lg);
        listOfTv.add(new TV());

        List<TV> filltredList = listOfTv.stream().filter(ob -> ob.getName().contains("l") && !ob.getName().equals("defaultName")).collect(Collectors.toList());
        List<String> tvNamesList = listOfTv.stream().map(ob -> ob.getName() + "Transformed").collect(Collectors.toList());

        tvNamesList.forEach(System.out::println);
        tvNamesList.forEach(ob -> System.out.println(ob));

    }
}
