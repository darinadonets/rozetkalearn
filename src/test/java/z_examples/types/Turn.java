package z_examples.types;

public enum Turn {
    ON("i switch ON"),
    OFF("i turn  OFF");

    private String name;

    Turn(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
