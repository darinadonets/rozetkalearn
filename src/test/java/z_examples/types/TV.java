package z_examples.types;

public class TV {

    public static String screenType = "LCD";
    public static final String POWER = "220";
//    public final String POWER2; не можливо перевизначити після створення класу


    public String name = "defaultName";
    private int diagonal;
    private RC remoteControl = new RC();
    private int channel;
    private int brightness;

    public TV(String name, int diagonal) {
        this.name = name;
        this.diagonal = diagonal;
//        POWER2 = "tth";
        System.out.println("TV created with name " + name + " and diagonal " + diagonal);
    }

    public TV() {
        System.out.println("Default TV");
    }

    public TV turn(Turn turn){
        remoteControl.turn(turn);
        return this;
    }

    public TV setChannel(int channel) {
        this.channel = channel;
        return this;
    }

    public int getChannel() {
        return channel;
    }

    public String getName() {
        return name;
    }
}
