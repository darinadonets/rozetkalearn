package z_examples.types;

import static z_examples.types.Turn.OFF;
import static z_examples.types.Turn.ON;

public class RC {

    private boolean turnOn;

    public void turn(Turn turn) {
        if (turn.equals(Turn.ON)) {
            System.out.println("I have receive".concat(ON.getName()));

            if (!turnOn) {
                System.out.println("TV turnOn");
                turnOn = true;
            } else {
                System.out.println("TV is already ON");
            }
        } else if (turn.equals(OFF)) {
            System.out.println("I have receive".concat(OFF.getName()));

            if (turnOn) {
                System.out.println("TV turnOff");
                turnOn = false;
            } else {
                System.out.println("TV is already OFF");
            }
        } else {
            throw new IllegalArgumentException("No other function is accepted");
        }

    }
}
