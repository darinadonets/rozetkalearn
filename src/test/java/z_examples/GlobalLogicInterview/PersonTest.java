package z_examples.GlobalLogicInterview;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class PersonTest {

    @Test
    public void personListTest() {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Peter", "Patterson", 21));
        personList.add(new Person("Paul", "Walker", 31));
        personList.add(new Person("Steve", "Runner", 41));
        personList.add(new Person("Arnold", "", 1));
        personList.add(new Person(" ", "Stevenson", 19));
        personList.add(new Person("Arnold", "Stevenson", 19));
        personList.add(null);

        personList.add(new Person("Aaron", "Bortnicker", 18));

        List<String> filteredList = personList.stream()
                .filter(Objects::nonNull)
                .map(Person::getName)
                .map(String::toUpperCase)
                .filter(StringUtils::isNoneBlank)
                .collect(Collectors.toList());
        System.out.println(filteredList);

        Person youngestPerson = personList.stream()
                .filter(Objects::nonNull)
                .min(Comparator.comparing(Person::getAge))
                .orElseThrow(NoSuchElementException::new);

        System.out.println(youngestPerson.toString());

    }
}
