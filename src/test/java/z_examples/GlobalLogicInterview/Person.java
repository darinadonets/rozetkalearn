package z_examples.GlobalLogicInterview;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class Person {

    String name;
    String surname;
    int age;

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        if (age < 0) {
            throw new IllegalArgumentException("Only positive numbers for person Age. Please try again");
        }
    }

    @Override
    public String toString() {
        return name.concat(" ").concat(surname).toUpperCase();
    }


}
