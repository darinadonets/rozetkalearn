package z_examples.GlobalLogicInterview;

import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

public class PersonOccupationTest {

    List<PersonOccupation> team = Arrays.asList(
            new PersonOccupation("Nikita", 21, Occupation.CEO),
            new PersonOccupation("Ruslan", 22, Occupation.CEO),
            new PersonOccupation("Dyadya Vanya", 60, Occupation.CLEANING_LADY),
            new PersonOccupation("Bob Martin", 46, Occupation.DEVELOPER),
            new PersonOccupation("Fred Brooks", 47, Occupation.DEVELOPER),
            new PersonOccupation("Maks", 21, Occupation.CEO)
    );


    @Test
    public void numberOfCeo() {
        // task 1 - get number of CEOs
        long ceo_num;
        ceo_num = team.stream()
                .filter(l -> l.getOccupation().equals(Occupation.CEO))
                .count();
        System.out.println("We have " + ceo_num + " CEOs running the business!");
    }

    @Test
    public void maxAgeInTeam() {
        // task 2 - get max age in team
        int max_age;
        PersonOccupation maxAge = team.stream()
                .max(Comparator.comparing(PersonOccupation::getAge))
                .orElseThrow(NoSuchElementException::new);
        max_age = maxAge.getAge();
        System.out.println("\nThe oldest person in team is only " + max_age);
    }

    @Test
    public void allPositionsExceptCeo() {
        // task 3 - get all team members per position, except CEO

        List<PersonOccupation> filteredPositions = team.stream()
                .filter(l -> !l.getOccupation().equals(Occupation.CEO))
                .collect(Collectors.toList());

        for(PersonOccupation person : filteredPositions) {
            System.out.print(person.getOccupation() +": " );
            System.out.println(person.getName());
        }

    }
}
