package z_examples.GlobalLogicInterview;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SortingTest {

    List<String> stringsToSort;
    List<Integer> integersToSort;

    @Test
    public void sortStrings() {
        stringsToSort = Arrays.asList("f", "h", "a", "p", "r", null, "", " ");
        List<String> sortedList = stringsToSort.stream()
                .filter(Objects::nonNull)
                .filter(StringUtils::isNoneEmpty)
                .filter(StringUtils::isNoneBlank)
                .filter(Predicate.not(String::isBlank))
                .sorted().collect(Collectors.toList());
        System.out.println(sortedList);
    }

    @Test
    public void sortStringWithNumbers() {
        stringsToSort = Arrays.asList("24", "3", "2", "3", "1");
        List<Integer> newList = stringsToSort.stream()
//                .map(s -> Integer.parseInt(s))
                .map(Integer::parseInt)
                .sorted()
                .collect(Collectors.toList());
        System.out.println(newList);
    }

    @Test
    public void sortIntegers() {
        integersToSort = Arrays.asList(1, 3, 5, 24, 12, 2);
        List<Integer> sortedList = integersToSort.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
        System.out.println(sortedList);
    }

}
