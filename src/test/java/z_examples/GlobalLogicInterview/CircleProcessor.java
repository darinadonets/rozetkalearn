package z_examples.GlobalLogicInterview;

    public class CircleProcessor {

        public static void main(String[] args) {
            Circle circle = new Circle();
            circle.setName("TEST NAME");

            fakeCircle(circle);
            System.out.println(circle.getName());
            System.out.println(circle.getColour());

        }
        private static Circle fakeCircle ( Circle circle )
        {

            circle.setName ( "FAKE NAME" ) ;

            circle = new Circle ( ) ;

            circle.setColour ( "RED" ) ;
            return circle ;

        }


        public static class Circle {
            String colour = "BLACK";
            String name = "CIRCLE";

            public String getColour() {
                return colour;
            }

            public void setColour(String colour) {
                this.colour = colour;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }