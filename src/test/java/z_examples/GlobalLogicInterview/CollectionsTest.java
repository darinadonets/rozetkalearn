package z_examples.GlobalLogicInterview;

import org.testng.annotations.Test;
import org.testng.collections.Lists;

import java.util.*;

public class CollectionsTest {

    @Test
    public void deleteDuplicates() {
        List<Integer> numbers = Lists.newArrayList(1, 2, 5, 3, 5, 2, 1, 1, 4, 7, 3, 2);
        List<Integer> listWithoutDuplicates = new ArrayList<>(new HashSet<>(numbers));
        // List<Integer> listWithoutDuplicates = numbers.stream()
        //     .distinct()
        //     .collect(Collectors.toList());
        System.out.println(listWithoutDuplicates);

    }

    @Test
    public void reverseNumber() {
        int reverse = 0;
        int num = 12345;
        while (num != 0) {
            reverse = reverse * 10;
            reverse = reverse + num % 10;
            num = num / 10;
        }
        System.out.println("Reversed number: " + reverse);
    }


    @Test
    public void addToListNotDuplicate() {
        List<String> colours1 = Lists.newArrayList("White", "Black", "Red");
        List<String> colours2 = Lists.newArrayList("Red", "Green");
        colours1.addAll(colours2);
        Set<String> filteredColoursList = new HashSet<>(colours1);
        System.out.println(filteredColoursList);
    }

    @Test
    public void sortMapViaKey() {

        Map<String, List<String>> map = new HashMap<>();
        map.computeIfAbsent("x", k -> new ArrayList<>()).add("4");
        map.computeIfAbsent("x", k -> new ArrayList<>()).add("2");
        map.computeIfAbsent("x", k -> new ArrayList<>()).add("1");
        map.computeIfAbsent("x", k -> new ArrayList<>()).add("3");

        System.out.println(map);
    }
}
