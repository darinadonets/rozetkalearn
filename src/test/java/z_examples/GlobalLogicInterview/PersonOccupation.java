package z_examples.GlobalLogicInterview;

import lombok.Getter;

@Getter
class PersonOccupation {

    private final String name;
    private final int age;
    private final Occupation occupation;

    public PersonOccupation(String name, int age, Occupation occupation) {
        this.name = name;
        this.age = age;
        this.occupation = occupation;
    }

    public int getAge() {
        return age;
    }

    public Occupation getOccupation() {
        return occupation;
    }

    @Override
    public String toString() {
        return name;
    }
}

enum Occupation {
    CEO,
    DEVELOPER,
    CLEANING_LADY

}

