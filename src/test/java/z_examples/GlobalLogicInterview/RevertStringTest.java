package z_examples.GlobalLogicInterview;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RevertStringTest {

    String sentence;
    @Test
    public void revertSimpleString(){
        sentence = "AB CD";
        StringBuilder sb = new StringBuilder(sentence);
        System.out.println(sb.reverse());
    }

    @Test
    public void hardStringRevert(){
        String sentence = "Selenium automates browsers. That's it! What you do  with that   power is entirely up to you.";
        String result = Stream.of(sentence.split("\\s+"))
                .map(word->new StringBuilder(word).reverse())
                .collect(Collectors.joining(" "));

        Assert.assertEquals("muineleS setamotua .sresworb s'tahT !ti tahW uoy od htiw taht rewop si yleritne pu ot .uoy",result);

    }
}
