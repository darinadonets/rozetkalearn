package z_examples;

import org.testng.annotations.Test;

import java.util.Random;

public class TRY {

    @Test(invocationCount = 5)
    public void exceptionscheck() {

        int catNumber;
        Integer zero;

        try {
            catNumber = 1;
            zero = new Random().nextInt(4);
            if (zero == 1) {
                zero = null;
            }

            int result = catNumber / zero;

            if (zero == 3) {
                ex3();
            }

            String[] catNames = {"Васька", "Барсик", "Мурзик"};
            catNames[3] = "Рыжик";
        } catch (NullPointerException e) {
            System.out.println("null");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("index");
        } catch (ArithmeticException e) {
            System.out.println("arith");
        } catch (Exception e) {
            System.out.println("exp");
        } finally {
            System.out.println("Finally block is executed");
        }


    }

    public void ex1() {
        throw new IllegalArgumentException();
    }

    public void ex2() {
        throw new IndexOutOfBoundsException();
    }

    public void ex3() throws Exception {
        throw new Exception();
    }

    public void ex4() {
        throw new NullPointerException();
    }


}
