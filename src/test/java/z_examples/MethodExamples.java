package z_examples;

import org.testng.annotations.Test;

public class MethodExamples {

    /**
     * public/ private/ ... - access modifier
     * void/ String/ Int/ Dog/ - type of return. !!!void mean that nothing is expected to be returned
     * method - name
     * (), (String),  (String, Int), (String, Dog) - received parameters
     * { ... } - body describe method actions/ behaviour
     * static/ final/ abstract key words   !!!public static void method
     */
    @Test
    public void method(){
        // Methods do not return nothing
        params0();
        params1("my first name");
        params1("my first name 2");
        params1("my first name ", "boo");

        //methods which return something
        System.out.println(return0());
        System.out.println(returnNumber("44"));

        int myReturnedInt = returnNumber("44");
        System.out.println(myReturnedInt);

        params1(return0().concat(" add | "), return0().concat(" second"));
    }

    public void params0(){
        System.out.println("nothing");
    }

    public void params1(String name){
        // make somne action with name or NOT (if not delete param if not in use)
        System.out.println("print ".concat(name));
    }

    public void params1(String name, String surname){
        System.out.println("print ".concat(name).concat(surname));
    }

    public void params1(String name, String surname, Integer age){
        params1(name, surname);
        System.out.println(age);
    }

//    public void params1(Integer name, String surname){
//        System.out.println("print ".concat(name.toString()).concat(surname));
//    }
//
//    public void params1(String name, Integer surname){
//        System.out.println("print ".concat(name).concat(surname.toString()));
//    }

    public String return0(){
        System.out.println("\ndo something in return");
        return "return 0";
    }

    public Integer returnNumber(String myNubmer){
        return Integer.valueOf(myNubmer);
    }

//    public Dog returnNumber(String myNubmer){
//        return new Dog(myNubmer);
//    }


}
